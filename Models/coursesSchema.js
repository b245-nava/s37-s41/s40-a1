const mongoose = require("mongoose");

const coursesSchema = new mongoose.Schema({
	name: {
		type: String,
		required: [true, "Please provide a name."]
	},
	description: {
		type: String,
		required: [true, "Please describe your new course."]
	},
	price: {
		type: Number,
		required: [true, "Please set a price."]
	},
	isActive: {
		type: Boolean,
		default: true
	},
	createdOn: {
		type: Date,
		// "new Date()" expression instatiates a new date that stores the current date and time whenever a course is created in our database.
		default: new Date()
	},
	enrollees: [
		{
			usedId: {
				type: String,
				required: [true, "userId is required."]
			},
			enrolledOn: {
				type: Date,
				default: new Date()
			}
		}
	]
});

module.exports = mongoose.model("Course", coursesSchema);