const express = require('express'); // Setting up server
const mongoose = require('mongoose'); // Setting up schemas + models
// By default, our backend's CORS will prevent any app outside our Express JS app to process the request. Using the CORS package will allow us to manipulate this and control what applications may use our app.

// CORS allows our backend application to be available to our frontend application.
// It allows us to control the app's Cross Origin Resource Sharing capabilities.
const cors = require('cors'); 

const userRoutes = require("./Routes/userRoutes.js");
const courseRoutes = require("./Routes/courseRoutes.js");


const port = 3001;
const app = express();

	mongoose.set('strictQuery', true);
	// MongoDB Connection
	mongoose.connect("mongodb+srv://admin:admin@batch245-nava.y30w9yr.mongodb.net/batch245_Course_API_Nava?retryWrites=true&w=majority", {
		useNewUrlParser: true,
		useUnifiedTopology: true
	});


	let db = mongoose.connection;

	// Error-handling
	db.on("error", console.error.bind(console, "Connection Error."));

	// Connection validation
	db.once("open", () => {console.log("We are connected to the cloud database.")});




// Middlewares 
app.use(express.json());
app.use(express.urlencoded({extended: true}));
app.use(cors());

// Routing
app.use("/user", userRoutes);
app.use("/course", courseRoutes);


app.listen(port, () => console.log(`Server is active at port ${port}.`));