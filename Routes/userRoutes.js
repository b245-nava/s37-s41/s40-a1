const express = require('express');
const router = express.Router();
const auth = require('../auth.js');

const userController = require('../Controllers/userController.js');


// [Routes]

// User Registration Route
router.post("/register", userController.userRegistration);

// User Authentication
router.post("/login", userController.userAuthentication);

// User Details
router.get("/details", auth.verify, userController.getUser);






module.exports = router;